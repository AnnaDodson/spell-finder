## Unofficial Harry Potter Spell Finding Web App

Find spells from Harry Potter and what they do by typing in what you want the spell to do.

- NodeJS and Express web app
- Bootstrap front end with Jquery and Ajax
- Using Microsoft LUIS natural processing API to convert free typed text into intents
- Intents and spell names and descriptions stored in [Redis](https://redis.io/) database for quick access

